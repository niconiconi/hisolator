Hisolator - A USB 2.0 High-Speed (480 Mbps) Galvanic Isolator.
================================================================

### Superseded by Newer Design

If you're looking for a High-Speed USB galvanic isolator, do not
waste your time looking at this repository. This project uses the
WCH CH317 ASIC, which has many performance and compatibility
limitations, making it unsuitable for many applications.

A newer design, using the new TI ISOUSB211 ASIC, is a much better
solution and does not have these limitations. It's available at:
[https://notabug.org/niconiconi/isousb211](https://notabug.org/niconiconi/isousb211)

This repository at this point is useful as a reference design for
the WCH CH317 ASIC.

### Motivation

USB 2.0 is one of the most commonly used data interfaces. Often, implementing
galvanic isolation (electrical isolation) is desirable. First, galvanic
isolation protects the computer from destruction by high voltage transients
or faults in an industrial environment. In the context of computing, it
can be used by embedded system developers and hardware hackers to protect
their computers from unexpected faults from the development boards, such as
a dangerous short-circuit from +12 V to +5 V. Given the widespread knowledge
of USB Killers, infosec researchers, too, can make use of an isolator. Also,
galvanic isolation is useful to stop unwanted conductive electromagnetic
interference between the computer and the USB device. Hence, amateur radio
operators with cheap, low-EMI-immunity USB software defined radios may also
find reliefs from noise with an USB isolator. And potentially, audio and video
engineers may find an isolator helpful for breaking a ground loop.

Unfortunately, although USB 2.0 isolators for Low Speed (1.5 Mbps) and Full
Speed (12 Mbps) are readily available, there are few isolators for High-Speed
USB (480 Mbps) interfaces due to the difficulty of its high data rate.
Currently, the available isolators are either expensive (FPGA-based solutions),
difficult to buy (ASIC isolators), or inconvenient to use (USB over CAT-5 or
fiber optics extenders, which requires a receiver and a transmitter).

This project is an experimental project that explores the possibility of
(ab)using the existing CH317 USB-to-Ethernet controller to implement a
single-board, low-cost, compact High-Speed USB 2.0 galvanic isolator,
without the high cost and complexity of a FPGA.

### Principle of Operation

The single-board PCB is physically separated into the primary (host) side
and secondary (device) side with an isolation barrier in between. Both sides
has independent power supplies and ground planes, and connected only by an
Ethernet transformer and nothing else. The transformer is designed for
Power-over-Ethernet application, thus it has a more robust Hi-Pot rating
(4 kV) than conventional transformers (1.5 kV). The transformer couples
the Ethernet traffic to the device side of the board. As proof-of-concept,
both sides use external power supplies and linear regulators to facilitate
debugging and development, no isolated DC-DC converters are used, yet, but
they will be introduced after the initial development is completed.

On the host side, the incoming USB traffic from the Micro-USB connector
is handled by CH317, acting as an USB PHY and USB controller. Subsequently,
CH317 acts a Gigabit Ethernet MAC and translate the USB traffic from the
computer to Ethernet frames, and drives a RTL8211F Gigabit Ethernet PHY via
RGMII to transmit the information on-the-wire. CH317 also controls (read/write
registers) on the RTL8211F PHY via a low-speed MDIO serial interface. 

The transformer couples the Ethernet traffic from the primary to the
secondary side of the board.

On the device side, the incoming Ethernet traffic is handled by another
RTL8211F and forwarded to another CH317, acting as an Ethernet MAC. The
Ethernet traffic is translated back to USB traffic and sent to the USB
device via an USB-A connector. This process is bidirectional, which means
the data can flow from host to device, or from device to host, thus
establishing bidirectional High-Speed USB communication.

To ensure the communication link can recover and reset if an unexpected
error has occurred (or if the USB is disconnected). A separate watchdog,
CH9317G, is used to monitor the USB traffic (via D+ and D- lines),
CH317 (via MDIO interface) and RTL8211F (via MDIO interface) simultaneously.

A four-layer PCB with controlled impedance is used to route high-speed
USB, RGMII and Ethernet signals. Unlike conventional four-layer PCBs,
there's no dedicated power plane but two ground planes - the stackup is
Signal/Power, Ground, Ground, Signal/Power with an extensive use of stitching
vias to provide a low-impedance current return path, thus minimizes its
interference to amateur radio receivers.

### Current Status

After some debugging, board v0.02 (see git tag) is functional and can
successfully establish a high-speed USB 2.0 link. Unfortunately, the data
rate is limited to less than 10 MB/s (80 Mbps) in benchmarks. Therefore, while
High-Speed USB 2.0 is technically supported, but its usefulness is limited,
and largely unusable by many USB 2.0 High-Speed devices.

It was suspected that it's an inherent limitation of CH317Q - which has already
been replaced by the second generation - CH317L. Thus, another prototype
(currently unrealesed) has been designed based on the newer CH317L to see whether
it could work as intended. Extra efforts were also made to produce a significantly
cleaner and better PCB layout to improve signal integrity.

Unfortunately, tests have showned that CH317L has essentially the same
performance. I can 100% confirm that the low performance was *not* a problem of
my design, but an inherent performance limitation of the CH317L ASIC - because I
found the **official** product from the manufacturer has the same performance
limitations!

Based on the observed low performance, the only feasible application is isolating
a USB flash drive, or something similar that doesn't require the full bandwidth of
USB 2.0 High Speed.

### TODO

* The new design based on CH317L is not released, since there are still minor
bugs in the PCB layout that requires manual patching, and I currently don't have
time to finalize it - because at this point it's useless for me at the personal
level. Nevertheless, I do realize that releasing my working design may still help
the community, especially when one considers that the relevant documentation is
not publicly available. Thus, I plan to design and test a final prototype, release
it for readers' reference, then permanently discontinue this project.

* But there is still hope. I'm now experimenting with an alternative chip for
USB isolation.

### Photos

#### New PCB waiting to be assembled on a PCB hot-plate heater and a hot-air gun.

![New PCB waiting to be assembled on a PCB hot-plate heater and a hot-air gun.](https://notabug.org/niconiconi/hisolator/raw/master/photos/board1.jpg)

#### Fully assembled and functional PCB.

![Fully assembled and functional PCB.](https://notabug.org/niconiconi/hisolator/raw/master/photos/board2.jpg)

#### Linux kernel detects a USB device on the isolator (it says full-speed, not high-speed, because the ST-Link device itself is full-speed only).

![Linux kernel detects a USB device on the isolator.](https://notabug.org/niconiconi/hisolator/raw/master/photos/board3.jpg)

### License

This is a free hardware design: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This hardware design is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with this hardware design. If not, see https://www.gnu.org/licenses/
