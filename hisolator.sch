EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3900 3550 3950 3500
U 5E73BE76
F0 "Host Side" 50
F1 "host.sch" 50
$EndSheet
Text Notes 6950 3150 0    197  ~ 39
Isolation Barrier
Wire Notes Line
	8150 3300 8150 7350
Wire Notes Line
	8250 3300 8250 7350
$Sheet
S 8600 3550 3950 3500
U 5EDD0BB0
F0 "Device Side" 50
F1 "device.sch" 50
$EndSheet
Text Notes 8850 6750 0    394  ~ 79
   USB\nDevice Side
Text Notes 4300 6800 0    394  ~ 79
   USB\nHost Side
Text Notes 900  10850 0    157  ~ 31
Copyright (C) 2020 Tom Li\n\nThis is a free hardware design: you can redistribute\nit and/or modify it under the terms of the GNU General\nPublic License as published by the Free Software Foundation,\neither version 3 of the License, or (at your option)\nany later version.\n\nThis hardware design is distributed in the hope that\nit will be useful, but WITHOUT ANY WARRANTY;\nwithout even the implied warranty of MERCHANTABILITY\nor FITNESS FOR A PARTICULAR PURPOSE. See the\nGNU General Public License for more details.
$EndSCHEMATC
